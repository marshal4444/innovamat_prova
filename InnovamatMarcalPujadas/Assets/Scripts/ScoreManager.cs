﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    private static ScoreManager _instance;

    public static ScoreManager Instance { get { return _instance; } }

    private static int aciertos;
    private static int fallos;

    private Text aciertosNumText;
    private Text fallosNumText;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;

            aciertos = 0;
            fallos = 0;
        }
    }

    public void Start()
    {
        aciertosNumText = GameObject.Find("AciertosNumber").GetComponent<Text>();
        fallosNumText = GameObject.Find("FallosNumber").GetComponent<Text>();

        aciertosNumText.text = aciertos.ToString();
        fallosNumText.text = fallos.ToString();
    }

    public void Score(bool isAcierto)
    {
        if (isAcierto)
        {
            aciertos++;
            aciertosNumText.text = aciertos.ToString();
        }
        else
        {
            fallos++;
            fallosNumText.text = fallos.ToString();
        }


    }

}
