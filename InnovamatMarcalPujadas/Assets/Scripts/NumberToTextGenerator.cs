﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumberToTextGenerator : MonoBehaviour
{

    private static NumberToTextGenerator _instance;

    public static NumberToTextGenerator Instance { get { return _instance; } }


    string[] unidades = new string[] { "cero", "uno", "dos", "tres", "cuatro", "cinco", "seis", "siete", "ocho", "nueve" };
    string[] teens = new string[] { "diez", "once", "doce", "trece", "catorce", "quince", "diceiséis", "diecisiete", "dieciocho", "diecinueve", "veinte" };
    string[] decenas = new string[] { "veinti", "treinta", "cuarenta", "cinquenta", "sesenta", "setenta", "ochenta", "noventa", "cien" };
    string[] centenas = new string[] { "ciento", "doscientos", "trescientos", "cuatrocientos", "quinientos", "seiscientos", "setecientos", "ochocientos", "novecientos", "mil" };

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }


    public string GetNumber(int number)
    {
        if (number < 10)
        {
            return unidades[number];
        } else if (number <= 20)
        {
            return teens[number - 10];
        } else if (number < 30)
        {
            int unidad = number % 10;

            string result = decenas[0];
            if (unidad != 0)
            {
                result += unidades[unidad];
            }

            return result;
        } else if (number <= 100)
        {
            int decena = number / 10;
            int unidad = number % 10;

            string result = decenas[decena - 2];
            if (unidad != 0)
            {
                result += " y " + unidades[unidad];
            }

            return result;
        } else if (number < 1000)
        {
            int centena = number / 100;
            int resto = number % 100;

            string result = centenas[centena-1];
            if (resto != 0)
            {
                result += " " + GetNumber(number % 100);
            }

            return result;
        } else if (number < 1000000)
        {
            int millares = number / 1000;
            int residuoCentenas = number % 1000;
            string result = GetNumber(millares) + " mil";

            if (residuoCentenas != 0)
            {
                result += " " + GetNumber(residuoCentenas);
            }

            return result;
        } else if (number < 2000000)
        {
            int residuoMillon = number % 1000000;
            string result = "un millón";
            if (residuoMillon != 0)
            {
                result += " " + GetNumber(number % 1000000);
            }

            return result;
        } else
        {
            int numeroMillon = number / 1000000;
            int residuoMillon = number % 1000000;
            string result = GetNumber(numeroMillon) + " millones";
            if (residuoMillon != 0)
            {
                result += " " + GetNumber(number % 1000000);
            }

            return result;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
