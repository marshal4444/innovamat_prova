﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;

    public static GameManager Instance { get { return _instance; } }

    private List<GameObject> buttonGameObjectList;
    private List<Animator> buttonAnimatorList;
    private List<Text> buttonTextList;
    private CanvasGroup buttonCanvasGroup;
    private Animator buttonCanvasAnimator;
    private GameObject textoEnunciadoGO;
    private Animator animatorEnunciado;
    private Text textoEnunciadoText;

    private int maxNum = 999999;
    private int erroresConsecutivos = 0;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        buttonGameObjectList = new List<GameObject>();
        buttonAnimatorList = new List<Animator>();
        buttonTextList = new List<Text>();
    }

    public void Start()
    {
        // Buscamos i guardamos los componentes del texto del enunciado y los botones
        GameObject buttonsParent = GameObject.Find("AnswerButtons");
        buttonCanvasGroup = buttonsParent.GetComponent<CanvasGroup>();
        buttonCanvasAnimator = buttonsParent.GetComponent<Animator>();
        textoEnunciadoGO = GameObject.Find("TextoEnunciado");
        animatorEnunciado = textoEnunciadoGO.GetComponent<Animator>();
        textoEnunciadoText = textoEnunciadoGO.GetComponent<Text>();

        // Seteamos el alpha del enunciado a 0.
        Color textoColor = textoEnunciadoText.color;
        textoColor.a = 0;
        textoEnunciadoText.color = textoColor;

        for (int i = 0; i < buttonsParent.transform.childCount; i++)
        {
            Transform childTransform = buttonsParent.transform.GetChild(i);
            buttonGameObjectList.Add(childTransform.gameObject);
            buttonAnimatorList.Add(childTransform.GetComponent<Animator>());
            buttonTextList.Add(childTransform.GetComponentInChildren<Text>());
        }

        // Llamamos la función para generar y mostrar un nuevo número en texto
        GetNewNumberOnScreen();
    }

    int answersAmount = 3;
    private List<int> randomNumbers;
    int randomRightAnswerIndex;
    private void GetNewNumberOnScreen()
    {
        // Escogemos 3 números por iteración (poca probabilidad de repetirse 1-1000000).
        int pickedNumbersCounter = 0;
        randomNumbers = new List<int>();
        while(pickedNumbersCounter < answersAmount)
        {
            int randomNumber = Random.Range(0,maxNum+1);
            for (int i = 0; i < pickedNumbersCounter; i++)
            {
                if (randomNumber == randomNumbers[i]) continue;
            }

            randomNumbers.Add(randomNumber);
            pickedNumbersCounter++;
        }

        // random de 3 para sacar la posición en la que irá la respuesta correcta.
        randomRightAnswerIndex = Random.Range(0, answersAmount);

        for (int i = 0; i < buttonTextList.Count; i++)
        {
            if (answersAmount >= i)
            {
                buttonTextList[i].text = randomNumbers[i].ToString();
            }
        }

        // Traducción del entero a letras.
        string newNumberText = NumberToTextGenerator.Instance.GetNumber(randomNumbers[randomRightAnswerIndex]);
        textoEnunciadoText.text = newNumberText;
        erroresConsecutivos = 0;
        ShowEnunciado();

        // Invoke de fadeout a 4 segundos, 2 de animación de fade in + 2 de verse en pantalla
        Invoke("HideEnunciado", 4);

        // Reseteamos el color de los botones y llamamos la función de mostrar respuestas cuando haya desaparecido el enunciado.
        for (int i = 0; i < buttonGameObjectList.Count; i++)
        {
            buttonGameObjectList[i].GetComponent<Image>().color = Color.white;
        }
        Invoke("ShowAnswers", 6);

        buttonCanvasGroup.alpha = 0f;
        buttonCanvasGroup.interactable = false;
    }

    private void ShowEnunciado()
    {
        animatorEnunciado.Play("FadeIn");
    }

    private void HideEnunciado()
    {
        animatorEnunciado.Play("FadeOut");
    }

    private void ShowAnswers()
    {
        buttonCanvasAnimator.Play("FadeInCanvasGroup");
        buttonCanvasGroup.interactable = true;
    }

    private void HideAnswers()
    {
        buttonCanvasAnimator.Play("FadeOutCanvasGroup");
    }

    public void SelectAnswer(Text answerText)
    {
        // Respuesta correcta
        if (answerText.text == randomNumbers[randomRightAnswerIndex].ToString())
        {
            ScoreManager.Instance.Score(true);

            SelectCorrectAnswer();
        }
        else // Respuesta incorrecta
        {
            ScoreManager.Instance.Score(false);
            erroresConsecutivos++;
            answerText.GetComponentInParent<Image>().color = Color.red;
            if (erroresConsecutivos == (answersAmount-1)) // Si es el segundo error
            {
                SelectCorrectAnswer();
            }
        }
    }

    // Marca en verde la respuesta correcta y pasa al siguiente enunciado.
    private void SelectCorrectAnswer()
    {
        buttonGameObjectList[randomRightAnswerIndex].GetComponent<Image>().color = Color.green;
        buttonCanvasGroup.interactable = false;
        Invoke("HideAnswers", 0.25f);
        Invoke("GetNewNumberOnScreen", 4);
    }
}
